import numpy as np
from base import k, number_of_plays, play_bandid

tau_default = 1.0


def softmax(q, tau=tau_default):
    return (np.exp(q / tau) / np.sum(np.exp(q / tau)))


def learn_softmax(tau=tau_default, number_of_plays=number_of_plays):
    mean_rewards = [0]
    q = np.zeros(k)
    count = np.zeros(k)
    for t in range(1, number_of_plays):
        pi_t = softmax(q, tau)
        a = np.random.choice(np.arange(k), p=pi_t)
        reward = play_bandid(a)
        count[a] += 1
        mean_reward = mean_rewards[-1] + ((reward - mean_rewards[-1]) / t)
        mean_rewards.append(mean_reward)
        q[a] = q[a] + (reward - q[a]) / count[a]
    return mean_rewards
