import random
import numpy as np
from fractions import Fraction

## config

winning_probs = [0.1, 0.3, 0.6, 0.4, 0.1, 0.4, 0.69, 0.71, 0.5, 0.2]
#                #0   #1   #2   #3   #4   #5   #6    #7    #8   #9
k = np.size(winning_probs)
number_of_plays = 1000
iterations = 250
epsilons = [1 / 128, 1 / 64, 1 / 32, 1 / 16, 1 / 8, 1 / 4, 1 / 2, 1]
taus = [1 / 16, 1 / 8, 1 / 4, 1 / 2, 3 / 4, 1, 5 / 4, 3 / 2, 2, 4, 8, 16]
cs = [1 / 2, 1, 2, 4, 8, 16]


## helper functions

def play_bandid(action: int):
    times_won = 0
    for _ in range(10):
        if random.random() < winning_probs[action]:
            times_won += 1
    return times_won


def calculate_average(function, values):
    return [np.average([np.average(function(value)) for iteration in range(iterations)]) for value in values]


def fraction_to_float(f):
    return float(sum(Fraction(s) for s in f.split()))
