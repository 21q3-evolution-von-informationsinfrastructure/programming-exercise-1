import matplotlib.pyplot as plt
from base import number_of_plays, epsilons, taus, cs, iterations, calculate_average, fraction_to_float
from epsilon_greedy import learn_epsilon_greedy
from softmax import learn_softmax
from upper_confidence_bound import learn_uppper_confidence_bound

# epsilon-greedy
print("Plotting epsilon-greedy...")
epsilon_greedy_values = calculate_average(learn_epsilon_greedy, epsilons)
plt.plot(epsilons, epsilon_greedy_values, label="ε-greedy")

# softmax
print("Plotting softmax...")
softmax_values = calculate_average(learn_softmax, taus)
plt.plot(taus, softmax_values, label="Softmax")

# upper confidence bound
print("Plotting upper confidence bound...")
ucb_values = calculate_average(learn_uppper_confidence_bound, cs)
plt.plot(cs, ucb_values, label="UCB")

plt.title(f'Comparison \n({iterations} iterations)')
plt.ylabel(f'Average reward over first {number_of_plays} steps')
plt.legend()
plt.semilogx(base=2)
ticks_as_string = ['1/128', '1/64', '1/32', '1/16', '1/8', '1/4', '1/2', '1', '2', '4', '8', '16']
test_as_float = list(map(fraction_to_float, ticks_as_string))
plt.xticks(test_as_float, ticks_as_string)
plt.show()
