import numpy as np
from base import k, number_of_plays, play_bandid

c_default = 1


def learn_uppper_confidence_bound(c=c_default, number_of_plays=number_of_plays):
    mean_rewards = [0]
    q = np.zeros(k)
    count = np.ones(k)
    for t in range(1, number_of_plays):
        a = np.argmax(q + c * np.sqrt(np.log(t) / count))
        reward = play_bandid(a)
        count[a] += 1
        mean_reward = mean_rewards[-1] + ((reward - mean_rewards[-1]) / t)
        mean_rewards.append(mean_reward)
        q[a] = q[a] + (reward - q[a]) / count[a]
    return mean_rewards
